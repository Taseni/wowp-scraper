#!/usr/bin/env python

from bs4 import BeautifulSoup
from datetime import datetime
from yattag import Doc, indent
import codecs
import concurrent.futures
import requests
import sys
import time


###  CONFIG ###
ITEM_LEVEL = 424 # minimum item level
NUM_PAGES = 5 # num pages to crawl through. attention: if a lot of chars are found and queried to raider.io, the raider.io api could temporarily ban your ip
BLACKLIST = ["Jozaku"]
### /CONFIG ###

WOWP_BASE_URL = 'https://www.wowprogress.com'
WOWP_LFG_URL = WOWP_BASE_URL + "/gearscore/de/char_rating/{}{}/lfg.1/raids_week.2/lang.de/sortby.ts"
NEXT = 'next/'
RIO_API_URL = "https://raider.io/api/v1/characters/profile?region=eu&realm={}&name={}&fields=raid_progression"

class Character:

    def __init__(self, name, realm, ilvl, guild, guild_link, date, link):
        self.name = name
        self.realm = realm
        self.ilvl = ilvl
        self.guild = guild
        self.guild_link = guild_link
        self.date = date
        self.link = link
        self.faction = ''
        self.race = ''
        self.klass = ''
        self.specs = []
        self.battle_tag = ''
        self.commentary = []
        self.raid1 = ''
        self.raid2 = ''

    def __str__(self):
        return 'Character: ' + self.race + ' ' + self.klass + ' ' + self.name + ' ( ' + WOWP_BASE_URL + self.link + ' )'

def load_url(url):
    r = requests.get(url, headers={'Connection': 'close'}, timeout=10)
    return r.content

def load_character_page_content(char):
    r = requests.get(WOWP_BASE_URL + char.link, headers={'Connection': 'close'}, timeout=10)
    return r.content, char

def fetch_rio(char):
    url = RIO_API_URL.format(char.realm.replace('\'', '-').replace(' ', '-'), char.name)
    response = requests.get(url, headers={'Connection': 'close'}, timeout=10)
    return response.json(), char

def scrape_lfg_table(html):
    soup = BeautifulSoup(html, 'lxml')
    rows = soup.find_all('tr')
    rows = rows[:23] # wowprogress has pretty broken tables but the first 23 tr are from the character list
    return rows

def analyse_row(row):
    td = row.find_all('td')
    ilvl_cell = td[4]
    ilvl = 0
    if ilvl_cell.string.find('--') == -1:
        ilvl = float(ilvl_cell.string)
        if ilvl <= ITEM_LEVEL:
            return None
    guild_cell = td[1].find('a')
    # if guild_cell is not None and 'horde' in guild_cell['class']:
    #     return None
    guild = guild_cell.string if guild_cell is not None else ""
    guild_link = guild_cell['href'] if guild_cell is not None else ""
    name_cell = td[0].find('a')
    name = name_cell.string
    if name in BLACKLIST:
      return None
    link = name_cell['href']
    realm_cell = td[3].find('a')
    realm = realm_cell.string
    date_cell  = td[5].find('span')
    date = datetime.fromtimestamp(int(date_cell['data-ts'])).strftime('%d.%m.%Y %H:%M:%S')
    return name, link, guild, guild_link, realm, ilvl, date

def scrape_character_page(html):
    soup = BeautifulSoup(html, 'lxml')
    battle_tag_span = soup.find('span', attrs={'class' : 'profileBattletag'})
    infos = soup.find_all('div', attrs={'class' : 'language'})
    commentary_div = soup.find('div', attrs={'class' : 'charCommentary'})
    transfer_idx = 1
    battle_tag = ''
    specs = []
    if battle_tag_span is not None:
        battle_tag = battle_tag_span.string
        transfer_idx = 2
    specs_idx = transfer_idx + 3
    transfer_span = infos[transfer_idx].find('span')
    specs_span = infos[specs_idx].find('strong')
    if transfer_span is not None and not 'ready' in transfer_span.string:
      return False, battle_tag, [], []
    if specs_span is not None:
        specs = specs_span.string.split(',')
    commentary = []
    if commentary_div is not None:
        for line in commentary_div.contents:
            line = str(line).replace('\n', '')
            if line and '<br' not in line:
                commentary.append(line)
    return True, battle_tag, specs, commentary 

def aggregate_rows():
    lfg_urls = [WOWP_LFG_URL.format('', '0')]
    for page in range(0, NUM_PAGES - 1):
        lfg_urls.append(WOWP_LFG_URL.format('next/', str(page)))
    rows = []
    for link in lfg_urls:
        html = load_url(link)
        rows.extend(scrape_lfg_table(html))
    return rows

def filter_rows(rows):
    chars = []
    for row in rows:
        result = analyse_row(row)
        if result is not None:
            name, link, guild, guild_link, realm, ilvl, date = result
            char = Character(name, realm, ilvl, guild, guild_link, date, link)
            chars.append(char)
    return chars

def check_rio(chars):
    filtered_chars = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        future_to_url = {executor.submit(fetch_rio, char): char for char in chars}
        for future in concurrent.futures.as_completed(future_to_url):
            race = ''
            klass = ''
            faction = ''
            try:
                data, char = future.result()
                if not 'error' in data:
                    race = data["race"]
                    klass = data["class"]
                    faction = data["faction"]
                    bod_progression = data["raid_progression"]["battle-of-dazaralor"]["summary"]
                    tep_progression = data["raid_progression"]["the-eternal-palace"]["summary"]
                    # if faction == 'alliance':
                    char.race = race
                    char.faction = faction
                    char.klass = klass.replace(' ', '').lower()
                    char.raid1 = tep_progression
                    char.raid2 = bod_progression
                    filtered_chars.append(char)
                # else:
                    # print(data)
            except Exception as exc:
                print("exception occured on", exc, data)
    return filtered_chars

def check_character_pages(chars):
    result = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        future_to_url = {executor.submit(load_character_page_content, char): char for char in chars}
        for future in concurrent.futures.as_completed(future_to_url):
            try:
                data, char = future.result()
                transfer, battle_tag, specs, commentary = scrape_character_page(data)
                if transfer:
                    char.battle_tag = battle_tag
                    char.specs = specs
                    char.commentary = commentary
                    result.append(char)
            except Exception as exc:
                print("exception occured on", exc, data)
    return result

# Create report as HTML file
def generate_report(chars): 
  file_name = 'Characters_{}.html'.format(datetime.now().strftime('%Y-%m-%d'))
  f = codecs.open(file_name, 'w', 'utf-8')
  f.write(generate_html(chars))
  f.close()

# Generate HTML of all characters
def generate_html(chars):
  doc, tag, text, line = Doc().ttl()
  doc.asis('<!DOCTYPE html>')
  with tag('html', lang='en'):
    with tag('head'):
      doc.asis('<meta charset="utf-8">')
      doc.asis('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">')
      line('title', 'WoWProgress LFG Scraper')
      doc.asis("""<style>
    body{background-color:#d3d3d3;background-image:url(https://bnetcmsus-a.akamaihd.net/cms/gallery/5EUH2IPCEQW71509559676922.jpg);background-attachment:fixed;background-position:50%;background-size:cover}p{margin-bottom:0}a:hover{text-decoration:none}.link-img{height:1.5em}.deathknight{color:#c41f3b}.demonhunter{color:#a330c9}.druid{color:#ff7d0a}.hunter{color:#abd473}.mage{color:#40c7eb}.monk{color:#00ff96}.paladin{color:#f58cba}.priest{color:#fff}.rogue{color:#fff569}.shaman{color:#0070de}.warlock{color:#8787ed}.warrior{color:#c79c6e}div.card-header.horde{border-bottom:5px solid #8C1616}div.card-header.alliance{border-bottom: 5px solid #144587}
</style>""")
    with tag('body'):
      with tag('div', klass='container'):
        line('h1', 'WoWProgress LFG Scraper', klass='text-light')
        doc.asis(generate_filter())
        with tag('div', klass='container'):
          for char in chars:
            doc.asis(generate_char_card(char))
      doc.asis('<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>')
      doc.asis('<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>')
      doc.asis("""<script>
        var classes = ["deathknight", "demonhunter", "druid", "hunter", "mage", "monk", "paladin", "priest", "rogue", "shaman", "warlock", "warrior"];
        var dict = {
            "deathknight": {
              "tank": ["tank"],
              "mdps": ["dd"]
            },
            "demonhunter": {
              "tank": ["tank"],
              "mdps": ["dd"]
            },
            "druid": {
              "tank": ["feral-tank"],
              "healer": ["restoration"],
              "mdps": ["feral-dd"],
              "rdps": ["balance"]
            },
            "hunter": {
              "rdps": ["beastmastery", "marksmanship"],
              "mdps": ["survival"]
            },
            "mage": {
              "rdps": ["arcane", "frost", "fire"]
            },
            "monk": {
              "tank": ["tank"],
              "healer": ["healer"],
              "mdps": ["dd"]
            },
            "paladin": {
              "tank": ["protection"],
              "healer": ["holy"],
              "mdps": ["retribution"]
            },
            "priest": {
              "healer": ["healer"],
              "rdps": ["dd"]
            },
            "rogue": {
              "mdps": ["assassination", "outlaw", "subtlety"]
            },
            "shaman": {
              "healer": ["restoration"],
              "mdps": ["enhancement"],
              "rdps": ["elemental"]
            },
            "warlock": {
              "rdps": ["affliction", "demonology", "destruction"]
            },
            "warrior": {
              "tank": ["protection"],
              "mdps": ["dd"]
            }
          };
          var roles = {
            "tank": "Tank",
            "healer": "Healer",
            "mdps": "Melee DPS",
            "rdps": "Range DPS"
          };
          var selectedRole = "none";
          var selectedClass = "none";
        $(document).ready(function() {
          $("#class-chooser").change(function() {
            selectedClass = $(this).children("option:selected").val();
            if (selectedClass === "none") {
              reset();
              return;
            }
            classes.forEach(klass => {
              if (klass === selectedClass) {
                $("div.card." + klass).show();
              } else {
                $("div.card." + klass).hide();
              }
            });
            updateRoleChooser(Object.keys(dict[selectedClass]));
          });
          $("#role-chooser").change(function() {
            selectedRole = $(this).children("option:selected").val();
            if (selectedClass === "none") {
              cardsToShow = [];
              for (let [key, value] of Object.entries(dict)) {
                Object.keys(value).forEach(role => {
                  if (role === selectedRole) {
                    value[role].forEach(r => {
                      cardsToShow.push(key + "." + r);
                    });
                  } else {
                    value[role].forEach( r => {
                      $("div.card." + r).hide();
                    });
                  }
                });
              }
              cardsToShow.forEach(card => {
                $("div.card." + card).show();
              });
              return;
            }

            if (selectedRole === "none") {
              $("div.card." + selectedClass).show();
              return;
            }
            var roleToShow = "";
            Object.keys(roles).forEach(role => {
              var dictRole = dict[selectedClass][role];
              if (dictRole != null) {
                if (role === selectedRole) {
                  roleToShow = dictRole;
                } else {
                  $("div.card." + dictRole).hide();
                }
              }
            });
            $("div.card." + selectedClass + "." + roleToShow).show();
          });
          $("#faction").change(function() {
            if (this.checked) {
              $("div.card.horde").hide();
            } else {
              $("div.card.horde").show();
            }
          });
          $(":reset").click(reset);
          $("span.btag a").click(copyToClipboard);
    });

    function updateRoleChooser(rolesOfClass) {
      $("#role-chooser").find("option").not(":first").remove();
      rolesOfClass.forEach(role => {
        var option = new Option(roles[role], role);
        $(option).html(roles[role]);
        $("#role-chooser").append(option);
      });
    };

    function reset() {
      $("div.card").show();
      updateRoleChooser(Object.keys(roles));
      selectedRole = "none";
      selectedClass = "none";
    };

    function copyToClipboard(event) {
      textarea = document.createElement("textarea");
			textarea.value = $(event.target).text().trim();
			document.body.appendChild(textarea);
			textarea.select();
			document.execCommand("copy");
			document.body.removeChild(textarea);
    };
    </script>""")
  return indent(doc.getvalue())
        
def generate_filter():
  doc, tag, text, line = Doc().ttl()
  with tag('div', klass='container bg-light border border-secondary rounded'):
    with tag('form'):
      with tag('div', klass='form-row align-items-center'):
        with tag('div', klass='col'):
          pass
        with tag('div', klass='col-auto my-2'):
          doc.input(name='faction', type='checkbox', klass='form-check-input', id='faction')
          line('label', 'Alliance Only', klass='form-check-label')
        with tag('div', klass='col-lg-3 my-2'):
          with doc.select(name='classes', id='class-chooser', klass='custom-select mr-lg-4'):
            for (value, desc) in (
              ('none', '-- Select Class --'),
              ('deathknight', 'Death Knight'),
              ('demonhunter', 'Demon Hunter'),
              ('druid', 'Druid'),
              ('hunter', 'Hunter'),
              ('mage', 'Mage'),
              ('monk', 'Monk'),
              ('paladin', 'Paladin'),
              ('priest', 'Priest'),
              ('rogue', 'Rogue'),
              ('shaman', 'Shaman'),
              ('warlock', 'Warlock'),
              ('warrior', 'Warrior')
            ):
              with doc.option(value = value):
                text(desc)
        with tag('div', klass='col-lg-3 my-2'):
          with doc.select(name='roles', id='role-chooser', klass='custom-select mr-lg-4'):
            for (value, desc) in (
              ('none', '-- Select Role --'),
              ('tank', 'Tank'),
              ('healer', 'Healer'),
              ('mdps', 'Melee DPS'),
              ('rdps', 'Range DPS')
            ):
              with doc.option(value = value):
                text(desc)
        with tag('div', klass='col-auto my-2'):
          line('button', 'Reset', klass='btn btn-dark', type='reset')
        with tag('div', klass='col'):
          pass
  return doc.getvalue()

# Generate HTML card for a character
def generate_char_card(char):
  doc, tag, text, line = Doc().ttl()
  card_klass = 'card my-2 bg-secondary text-light rounded-0 ' + char.klass + ' ' + ' '.join(char.specs) + ' ' + char.faction
  with tag('div', klass=card_klass):
    div_klass = 'card-header ' + char.faction
    with tag('div', klass=div_klass):
      with tag('div', klass='row'):
        with tag('div', klass='col-auto'):
          h5_klass = 'text-uppercase font-weight-bold ' + char.klass
          line('h5', char.name, klass=h5_klass)
        with tag('div', klass='col'):
          line('p', char.realm, klass='font-italic')
        with tag('div', klass='col-auto'):
          with tag('span', klass='btag'):
            line('a', char.battle_tag, klass='font-weight-bold text-light', href='#', title='Copy to Clipboard')
        with tag('div', klass='col-auto'):
          url = WOWP_BASE_URL + char.link
          wcl_url = 'https://www.warcraftlogs.com/character/eu/' + char.realm.replace(' ', '-').replace('\'','') + "/" + char.name
          rio_url = 'https://raider.io/characters/eu/' + char.realm.replace(' ', '').replace('\'','') + "/" + char.name
          with tag('a', href=url):
            doc.stag('img', klass='link-img', src='https://media.forgecdn.net/avatars/89/102/636229408134215920.png')
          with tag('a', href=wcl_url):
            doc.stag('img', klass='link-img mx-1', src='https://dmszsuqyoe6y6.cloudfront.net/img/warcraft/favicon.png')
          with tag('a', href=rio_url):
            doc.stag('img', klass='link-img', src='https://cdnassets.raider.io/images/brand/Icon_2ColorWhite.png')
    with tag('div', klass='card-body'):
      with tag('div', klass='row'):
        with tag('div', klass='col-lg-4'):
          with tag('p'):
            line('span', 'Progression: ', klass='font-italic mr-2')
            text(char.raid1 + ' ' + char.raid2)
          with tag('p'):
            line('span', 'Race: ', klass='font-italic mr-2')
            text(char.race)
          with tag('p'):
            line('span', 'Specs: ', klass='font-italic mr-2')
            text(', '.join(char.specs))
          with tag('p'):
            line('span', 'Itemlevel: ', klass='font-italic mr-2')
            text(char.ilvl)
          with tag('p'):
            line('span', 'Last Guild: ', klass='font-italic mr-2')
            guild_url = WOWP_BASE_URL + char.guild_link
            line('a', char.guild, href=guild_url)
          with tag('p'):
            line('span', 'Search added: ', klass='font-italic mr-2')
            text(char.date)
        with tag('div', klass='col-lg-8'):
          if len(char.commentary) > 0:
            with tag('p', klass='card-text p-1 bg-light text-dark'):
              for comm in char.commentary:
                text(comm)
                doc.asis('<br/>')
  return doc.getvalue()

## RUN ##
start = time.time()
print('Fetch characters from WoWProgress LFG tables...')
rows = aggregate_rows()
chars = filter_rows(rows)
print('Fetch infos from raider.io for {} characters...'.format(len(chars)))
start1 = time.time()
filtered_chars = check_rio(chars)
runtime = time.time() - start1
print('This took {:.2f} seconds'.format(runtime))
# print('{} alliance characters left'.format(len(filtered_chars)))
print('Fetch recruitment info from WoWProgress character pages...')
start2 = time.time()
result = check_character_pages(filtered_chars)
runtime = time.time() - start2
print("This took {:.2f} seconds".format(runtime))
runtime = time.time() - start
print('Result: {} valid characters found in {:.2f} seconds.'.format(len(result), runtime))
print('Generate report... ')
generate_report(result)
print('Done.')