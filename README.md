# WOWProgress Scraper
This script scrapes the player lfg list of [WoWProgress.com](https://www.wowprogress.com/) to find new potential applicants for guild recruitment.

## Configuration
You can configure following parameters inside the script:

* the minimum itemlevel
* the main realm
* if you want to find only alliance or alliance AND horde characters
* the number of pages to scrape

## Run the script
You need [Python 3.7 or higher](https://www.python.org/downloads/).

Furthermore you have to install via `pip`:

* `pip install requests`
* `pip install bs4`
* `pip install lxml`
* `pip install yattag`

Run the script with `python scrape.py` and open the created HTML-File.